import java.util.*;

public class HitungCharacterApp {
    public static void main(String[] args) {
        hitungVocal();
    }

    private static void hitungVocal(){
        int nVocal = 0;
        int nCons = 0;
        int nAngka = 0;
        int nSpace = 0;
        int nSpecialchar = 0;

        Scanner inp = new Scanner(System.in);
        System.out.print("Masukkan Kalimat : ");
        String kalimat = inp.nextLine();

        // Convert inputan user menjadi Uppercase semua
        kalimat = kalimat.toUpperCase();

        // Perulangan Cek Huruf
        for (int i = 0; i < kalimat.length() ; i++) {
            char huruf= kalimat.charAt(i);

            if(huruf=='A' || huruf=='I' || huruf=='U' || huruf=='E' || huruf=='O'){
                nVocal++;
            } else if(huruf >='A' && huruf <='Z') {
                nCons++;
            } else if(huruf >= '0' && huruf<='9') {
                nAngka++;
            } else if(huruf==' '){
                nSpace++;
            } else {
                nSpecialchar++;
                }
            }

        System.out.println("-----------------------------------------------");
        System.out.println(kalimat);
        System.out.println("-----------------------------------------------");
        System.out.println("Total Karakter         : " + kalimat.length());
        System.out.println("Jumlah Huruf Vokal     : " + nVocal);
        System.out.println("Jumlah Huruf Konsonan  : " + nCons);
        System.out.println("Jumlah Char Angka      : " + nAngka);
        System.out.println("Jumlah Char Spasi      : " + nSpace);
        System.out.println("Jumlah Char Spesial    : " + nSpecialchar);
        }
}
